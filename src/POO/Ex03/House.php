<?php
  
namespace App\POO\Ex03;

// On crée une classe abstract pour utiliser des fonctions abstractes
abstract class House {
    
    /* Les fonctions abstractes forment un lien avec les fonctions définient
    dans les classes enfants*/
    abstract function getHouseName();
    abstract function getHouseSeat();
    abstract function getHouseMotto();

    // Fonction introduce qui est appelée par le test
    public function introduce(){
        /* on echo le message demandé par le readme en
        mélangeant des mots avec des valeurs obtenues par
        les classes enfants*/
        echo 'House ';
        echo $this->getHouseName();
        echo ' of ';
        echo $this->getHouseSeat();
        echo ' : ';
        echo '"'.$this->getHouseMotto().'"'."\n";
    }
}
