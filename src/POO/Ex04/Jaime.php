<?php

namespace App\POO\Ex04;

// Toutes les classes appelées dans la classe Jaime
use App\Resources\Classes\Lannister\Lannister;
use App\POO\Ex00\Tyrion;
use App\Resources\Classes\Stark\Sansa;
use App\Resources\Classes\Lannister\Cersei;

// Classe Jaime enfant de la classe Lannister
class Jaime extends Lannister {

    // Les constantes associées aux messages qui apparaissent selon le partenaire
    public const DRUNK="Not even if I'm drunk !\n";
    public const DO="Let's do this.\n";
    public const PLEASURE="With pleasure, but only in a tower in Winterfell, then.\n";

    // La fonction publique qui sera employée par le test, qui utilise le paramètre partenaire
    public function sleepWith($partenaire) {

        // Si le partenaire est Tyrion, alors on affiche le message drunk
        if ($partenaire instanceof Tyrion) {
            echo self::DRUNK;
        // Si le partenaire est Sansa, alors on affiche le message do
        } elseif ($partenaire instanceof Sansa) {
            echo self::DO;
        // Si le partenaire est Cersei, alors on affiche le message pleasure
        } elseif ($partenaire instanceof Cersei) {
            echo self::PLEASURE;
        // Finalement on affiche une erreur si aucun partenaire correspond
        } else {
            echo "error \n";
        }
    }
} // Bien joué je n'ai pas utilisé cette fonction instanceof elle est plus simple d'utilisation que ma méthode.