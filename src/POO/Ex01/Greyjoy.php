<?php
namespace App\POO\Ex01;

/* La variable $familyMotto de la classe Greyjoy est employée
dans la méthode d'Euron.php, il suffit donc juste de lui associer
la valeur "We do not sow" */
class Greyjoy {
    protected $familyMotto = "We do not sow";
}