<?php

namespace App\POO\Ex05;

class NightsWatch {

    // On crée une variable recrues qui est un tableau
    public $recrues;

    /* La fonction recruit() prend comme paramètre "personnage" qui prendra la valeur des différents personnage du test */
    public function recruit($personnage)
    {
        // On rentre les personnages dans le tableau recrues
        $this->recrues[] = $personnage;
    }

    // Fonction fight qui vérifie si les recrues sont capables de se battre
    public function fight()
    {
        // Pour chaque recrue du tableau
        foreach ($this->recrues as $recrue) {

            /* Si dans la recrue il existe la fonction "fight", alors cette recrue est un combattant */
            if (method_exists($recrue, 'fight')) {
                
                // Donc on affiche le message se trouvant dans sa classe 
                $recrue->fight();
            }
        }
    }
}