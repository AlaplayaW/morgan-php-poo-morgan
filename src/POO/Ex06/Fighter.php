<?php

namespace App\POO\Ex06;

abstract class Fighter {
    public $type;
    // On crée le constructeur
    
    public function __construct($soldier_type) {
        // On affecte les différentes classes enfant à ce constructeur
        $this->type = $soldier_type;
    }

    abstract public function fight(string $target = ''): void;


}