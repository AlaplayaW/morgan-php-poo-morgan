<?php

const ERROR = "Wrong Format\n";
// Je définie une constante associée à un message d'erreur

if ($argc == 2) {
    // Je vérifie si il y a bien un paramètre rentré

    $chaine = $argv[1];
    // J'associe le paramètre rentré à une variable

    if (preg_split("/\s+/", $chaine, -1, PREG_SPLIT_NO_EMPTY) != false) {
        // Si le résultat du preg_split n'est pas égal à false alors...

        $tab = preg_split("/\s+/", $chaine, -1, PREG_SPLIT_NO_EMPTY);
        // On casse la chaîne pour assigner chaque valeur à un tableau

        $jour = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];
        // On crée un tableau pour tous les jours de la semaine
        $mois = ['Janvier' => 1, 'Fevrier' => 2, 'Mars' => 3, 'Avril' => 4, 'Mai' => 5, 'Juin' => 6, 'Juillet' => 7, 'Aout' => 8, 'Septembre' => 9, 'Octobre' => 10, 'Novembre' => 11, 'Decembre' => 12];
        // On crée un tableau pour tous les mois de l'année

        if (in_array(ucfirst(strtolower($tab[0])), $jour) && array_key_exists(ucfirst(strtolower($tab[2])), $mois)) {
            // On vérifie que le jour et le mois sont biens écris

            $Chiffrejour = $tab[1];
            $Chiffremois = $mois[ucfirst(strtolower($tab[2]))];
            $ChiffreAnnee = $tab[3];
            // On associe le jour, mois et année à des variables

            $temps = preg_split('/:/', $tab[4], -1, PREG_SPLIT_NO_EMPTY);
            // On split l'heure stocké dans le tab[4] afin de vérifier qu'elle soit correcte

            if ($temps[0] < 24 && $temps[1] < 60 && $temps[2] < 60) {
                // On vérifie si elle est bien correcte

                $heure = true;
            // Si elle est correcte la variable heure devient true
            } else {
                $heure = false;
                // Sinon elle devient false
            }

            if (checkdate($Chiffremois, $Chiffrejour, $ChiffreAnnee) && ($heure == true)) {
                // On formate la date pour que la fonction DateTime la comprenne
                $nom = $Chiffremois . '/' . $Chiffrejour . '/' . $ChiffreAnnee . ' ' . $temps[0] . ':' . $temps[1] . ':' . $temps[2];
                // On rentre la date dans la variable $nom
                $date = new DateTime($nom, new DateTimeZone('CET'));

                $result = $date->getTimestamp();
                // On utilise getTimestamp afin d'obtenir le résultat en secondes
                echo $result . "\n";
            // On l'affiche
            } else {
                echo ERROR;
                // Résultat du if si la condition n'est pas remplie
            }
        } else {
            echo ERROR;
            // Résultat du if si la condition n'est pas remplie
        }
    } else {
        echo '';
        // Ici le if ne renvoie pas d'erreur parce qu'il ne reçoit rien.
    }
} else {
    echo ERROR;
}
// //Résultat du if si la condition n'est pas remplie
