<?php

$tableau_numerique = [];
$tableau_lettres = [];
$tableauspeciaux = [];
// Nous déclarons pour plus tard 3 tableaux associés à chaque type de caractère.

$argv1 = array_slice($argv, 1);
// On récupère les valeurs entrées dans le premier paramètre dans l'array $argv1
$mots = implode(' ', $argv1);
// On explose le tableau en string afin de séparer les cas de plusieurs
// mots contenus en une seule valeur (ex: "1948372 AhAhAh")
$tableau = preg_split("/\s/", $mots, -1, PREG_SPLIT_NO_EMPTY);
// On utilise preg_split afin de tout remettre sous la forme de tableau

foreach ($tableau as $value) {
    if (ctype_alpha($value)) {
        $tableau_lettres[] = $value;
    }
    // On rentre tous les mots dans un tableau associé aux mots uniquement grâce à ctype_alpha
    elseif (ctype_digit($value)) {
        $tableau_numerique[] = $value;
    }
    // On fait de même pour les nombres grâce à ctype_digit
    else {
        $tableauspeciaux[] = $value;
    }
    // Pour finir tout ce qui n'est pas un mot ou un nombre rentre dans la dernière catégorie
}
sort($tableau_numerique, SORT_STRING);
natcasesort($tableau_lettres);
sort($tableauspeciaux);
// On trie les trois tableaux de la façon que l'on souhaite.

$tableauclasse = array_merge($tableau_lettres, $tableau_numerique, $tableauspeciaux);
// On fusionne les trois tableaux en un à l'aide d'array_merge
$resultatfinal = implode("\n", $tableauclasse) . "\n";
// On le fait passer en string;
echo $resultatfinal;
// On l'affiche
