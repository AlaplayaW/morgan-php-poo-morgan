<?php

// Si le mot de passe est vide on retourne une erreur
if ($_POST['submit'] != 'OK' || $_POST['login'] == null || $_POST['passwd'] == null) {
    echo "ERROR\n";

    return;
}
$file = '../ex21/private/passwd';

// La variable login récupère le login et password
$login = ['login' => $_POST['login'],
            'passwd' => password_hash($_POST['passwd'], PASSWORD_DEFAULT), ];
if (file_exists($file)) {
    $users = file_get_contents($file);
    $users = unserialize($users);
    foreach ($users as $user) {
        if ($user['login'] == $_POST['login']) {
            echo "ERROR\n";

            return;
        }
    }
}
$users[] = $login;
$serial = serialize($users);
if (!file_exists($file)) {
    mkdir('../ex21/private', 0777);
}
if (!file_put_contents($file, $serial)) {
    echo "ERROR\n";

    return;
} else {
    echo "OK\n";
}
