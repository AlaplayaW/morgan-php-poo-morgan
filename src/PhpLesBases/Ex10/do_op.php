<?php

// Récupérer 3 paramètres (argv) pas +
// Le 1er et le 3e DOIVENT être des nombres
// Le second DOIT être une opération arithmétique

$errormsg = "Incorrect Parameters\n";
// On déclare le message d'erreur pour le réutiliser plus tard

$tableauparametre = array_slice($argv, 1);
// On récupère la liste des paramètres sans prendre le code
if (count($tableauparametre) != 3) {
    echo $errormsg;
    exit();
}
// Si nous avons plus de trois paramètres, le programme se termine

$tableauparametre[0] = trim($tableauparametre[0]);
$tableauparametre[1] = trim($tableauparametre[1]);
$tableauparametre[2] = trim($tableauparametre[2]);
// On enlève les espaces des trois paramètres converti dans le tableau

if (is_numeric($tableauparametre[0]) && is_numeric($tableauparametre[2])) {
    // Si les paramètres 1 & 3 sont des nombres alors :
    switch ($tableauparametre[1]) {
        // Si le paramètre 2 est '+' alors...
        case '+':
            echo $tableauparametre[0] + $tableauparametre[2];
            break;
        case '-':
            echo $tableauparametre[0] - $tableauparametre[2];
            break;
        case '*':
            echo $tableauparametre[0] * $tableauparametre[2];
            break;
        case '/':
            if ($tableauparametre[2] == 0) {
                echo '0';
            } else {
                echo $tableauparametre[0] / $tableauparametre[2];
            }
            break;
        case '%':
            echo $tableauparametre[0] % $tableauparametre[2];
            break;
        default:
            echo $errormsg;
    }
    echo "\n";
// Saut de ligne qui s'applique à toutes les possibilités de messages
} else {
    echo $errormsg;
    exit();
    // Sinon on affiche le message d'erreur et on quitte le programme
}
