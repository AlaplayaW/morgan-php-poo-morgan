<?php

$text = file_get_contents($argv[1]);
// On utilise la fonction file_get_contents afin de récupérer le contenu d'un fichier

preg_match_all("/<a.*>.*<\/a>/", $text, $tabliens);
// On trouve les liens "a href" grâce aux regex et on les insère dans un tableau de liens

foreach ($tabliens[0] as $lien) {
    // On crée une boucle pour chaque lien du tableau

    preg_match('/>.*</', $lien, $tab);
    // On cherche le texte se trouvant entre les chevrons "<>"

    $final = preg_replace('/<.*>/', '', $tab[0]);
    // On met du vide entre les chevrons

    $gras = strtoupper($final);
    // On met le texte en majuscule

    $text = str_replace($final, $gras, $text);
    // On remplace le texte du lien en gras
}

preg_match_all('/title=".*"/', $text, $tabliens);
// On cherche les parties du lien avec "title= "

foreach ($tabliens[0] as $title) {
    $mot = str_replace('title', '', $title);
    // On remplace les title par des espaces vides
    $mot = strtoupper($mot);
    // On met en majuscules
    $mot = 'title' . $mot;
    // ???
    $text = str_replace($title, $mot, $text);
    // On remplace la variable $title par $mot
}

    echo $text;
